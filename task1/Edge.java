/**
 * Edge class.
 * @author juliahu
 *
 */
public class Edge implements Comparable<Edge> {
    Car car1;
    Car car2;
    int distance;
    
    /**
     * edge constructor.
     * @param c1 is car 1.
     * @param c2 is car 2. 
     * @param d is distance.
     */
    public Edge(Car c1, Car c2, int d) {
        this.car1 = c1;
        this.car2 = c2;
        this.distance = d;
    }
    /**
     * get distance.
     * @return distance.
     */
    public int getD() {
        return this.distance;
    }
    
    @Override
    public int compareTo(Edge otherItem) {
        if (this.distance == otherItem.distance) {
            return 0;
        } else if (this.distance > otherItem.distance) {
            return 1;
        } else if (this.distance < otherItem.distance) {
            return -1;
        } else {
            return 0;
        }
    }
    
    //@Override
//    public boolean equals(Object otherItem) {
//        
//        if (otherItem instanceof Edge) {
//            Edge temp = (Edge) otherItem;
//            if (this.distance == temp.distance) {
//                
//                return true;
//            } else {
//                return false;
//            }
//        } else {
//            return false;
//        }
//    }
}
