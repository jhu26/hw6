Julia Hu, Anson Shen, Michael Gao
=======================
Task 1
=======================

In order to solve Backyard Dig, we used Kruskal's algorithm to find the 
min spanning tree. In order to do so, used an Edge class to store the 
two cars and the distance to make up that edge. All these edge classes were
then stored into a min Heap so that we can efficiently retrieve the min distance
to add to our min spanning tree. We kept adding distances if the distance added
 1) didn't create a cycle or 2) the two cars were already in the same cycle. We used a hashmap to store the name of each set for each car. (i.e. car (0,1) 's set name is 1 and car (1,0)'s set name is 2.) These union relationships were 
then represented in the union class. I used find(car's set name) to find out 
if the two cars were in the same set. 

===================================
Task 2: CodeSort Readme
===================================

After reading the Task 2 requirement, we immediately thought about using a 
topological sort. We want to use a topological sort because from the 
smallSorted.txt file, we can get many pairs of relative orderings for characters.
We can easily represent and the these relative/partial orderings with a directed 
graph. And after we have the graph, the most efficient ways to get an absolute 
ordering for these characters is to use a topological sort. Once we have an
absolute ordering, we can sort the unsorted.txt file using a sort method and output
the desired file.

Graph Class:

First, to store all the characters in a graph, we wrote a graph class. Inside the 
graph class, there are a vertex class and a edge class. The graph class has an ArrayList
of vertices. The reason why we chose to use an ArrayList is because it’s the best fit for
our program. A HashSet can be a choice for other teams. However, for our program, we want 
to traverse through the list and get the actual vertices when we need them. A HashSet 
will be able to give us better running time overall but it wouldn’t be able to return the 
actual vertex and the position of it when we need it. In addition, we also have two int
numv, which keeps track of the number of vertices and nume, which keeps track of the number
of edges.There are a method to get the vlist, a checker method to print everything
in the vlist, a method to add vertex, a method to add edge, a method to return numv,
a method to return nume, a contains vertex method, a delete vertex method, a delete
edge method.

Vertex Class:

In our vertex class, we have three data fields: one ArrayList of incoming edges, one 
ArrayList of outgoing edges and a String name. we have an add incoming edge method,
an add outgoing edge method, a get name method, a delete outgoing edge method, a
delete incoming edge method, a get ArrayList incoming method, a get ArrayList outgoinging 
method, a delete all outgoing edges method.

Edge Class:

In our edge class, we have two data fields: one Vertex from and one vertex to. We have
a method to get the vertex from and a method to get the vertex to.


CodeSort Class:

In our CodeSort class, we have an ArrayList of string, sortedwords, which stores all
the words we get from our first file, an ArrayList of string, unsortedwords, which has 
all the unsorted words we get from the second file and which we sort later, an array of
string sorted, which stores the ordered characters, a graph g, a HashMap<String, Integer>
h which we store the characters with the priority value into. The reason why we use a
HashMap is because we don’t care about order of things (etc. LinkedList, ArrayList, but we
care about the easy access of value by key.

In our main, we first read in the sorted words into sorted words and unsorted words into
unsorted words, then we store all the characters into a directed graph g. Afterwards, we
do a topological sort to figure out the absolute order of characters and store the order
in an array list, which we then store into a HashMap h. Then, we conduct a bubble sort on
the unsortedwords list. After we are done with that, we just output the sorted list to the
third file.

Note:

Technically, we don’t need the two ArrayList, as we don’t have to store all the sorted 
characters and we can just process all the characters from and store them into the 
HashMap. However, to break up the methods and make the code easily understandable. 
we decided to include the two arraylists.


========================================
Task 3:
========================================

* compile:
make

* run: 
java Taxi 3 mapLocations.txt mapConnections.txt driverLocations.txt 
java Taxi 3 mapLocations2.txt mapConnections2.txt driverLocations2.txt 

Description:
Note: Many things are key'd on the location name. I avoid 
using a vertex class as the key because I change the info stored within the vertex
class.

I represent my undirected graph within my Graph class 
with an adjacency list--implemented with a
hashmap with the key as string (location name) and value as a linked list of 
vertices (Loc class). In addition, my graph class is also implemented
with a hashmap with key as the location name but maps to the exact
Loc class as the value.

I implement the weights of my graph in a class called Traffic, which
is implemented with a hashmap of the location pair represented as a String
and the value being the distance. This "weight function" is used in
my dijkstra.

I also keep track of my drivers in the similar way--a hashmap key'd
on the location name and value being a linkedlist of Driver classes. 

Now, to solve the problem I first form my graph and initialize the necessary variables.
Then, I run dijkstra and solve the single source shortest paths problem
with the source being the pickup location. All nodes are updated with 
a parent pointer that will lead back to the source. Furthermore, they are
updated with the min-distance field, which stores the minimum distance (travel time)
from that location. The weights are gotten from the Traffic class in O(1).

After the dijkstra run, all the heavy lifting is done. 
To find the k nearest drivers, I sort my locations on the minium distance
field. Then, I loop through each location, grabbing drivers until I get k
drivers.

From the location of the selected driver, I simply follow the parent pointers
back to the source and there is my route. 


