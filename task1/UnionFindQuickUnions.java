/**
 * Union Find Quick Unions.
 * @author juliahu
 *
 */
public class UnionFindQuickUnions implements UnionFind {
    /**
     * default size.
     */
    public static final int DEFAULT_SIZE = 10;
    /**
     * current size.
     */
    int currentSize = 0;
    /**
     * array of nodes.
     */
    private int [] arrayInt;
    /**
     * number of subsets.
     */
    private int numSubset;
   
    /**
     * QuickUnions Constructor.
     */
    public UnionFindQuickUnions() {
        this.currentSize = DEFAULT_SIZE;
        this.arrayInt = new int[DEFAULT_SIZE];
        this.numSubset = DEFAULT_SIZE;
        
        for (int i = 0; i < this.currentSize; i++) {
            this.arrayInt[i] = -1;
        }
    }
    
    /**
     * constructor with argument.
     * @param size = current
     */
    public UnionFindQuickUnions(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException();
        }
        this.currentSize = size;
        this.arrayInt = new int[size];
        this.numSubset = size;
        for (int i = 0; i < this.currentSize; i++) {
            this.arrayInt[i] = -1;
        }
    }
    
    /**
     * Determine the name of the set containing the specified element.
     * @param x the element whose set we wish to find
     * @return the name of the set containing x
     */
    public int find(int x) {
  
        if (!(this.arrayInt[x] < 0)) {
            this.arrayInt[x] = this.find(this.arrayInt[x]);
        }
        
        if (this.arrayInt[x] >= 0) {
            return this.arrayInt[x];
        }
        return x;      
    }
    /**
     * Merge two sets if they are not already the same set.
     * @param a an item in the first set to be merged (need not be set name)
     * @param b an item in the second set to be merged (need not be set name)
     */
    public void union(int a, int b) {
        int aFound = this.find(a);
        int bFound = this.find(b);
        
        if (aFound == bFound) {
            return;
        }

        if (this.arrayInt[aFound] < this.arrayInt[bFound]) {
            // if negative number and need to update size
            int tempSize = this.arrayInt[bFound];
            this.arrayInt[bFound] = aFound;
            this.arrayInt[aFound] = this.arrayInt[aFound] + tempSize;   
            
        } else { // if b is bigger than a
            int tempSize = this.arrayInt[aFound];
            this.arrayInt[aFound] = bFound;
            this.arrayInt[bFound] = this.arrayInt[bFound] + tempSize;
              
        }
        this.numSubset--;
    }
    
    /**
     * Return the number of subsets in the structure.
     * @return the number of subsets
     */
    public int getNumSubsets() {
        return this.numSubset;
    }
    
    /**
     * Returns a String representation of the implementation.  Normally
     * this would never be part of an interface like this, but will help us
     * test your implementation in a consistent way.  See assignment handout.
     * @return a String representing the current state of the structure
     */
    public String getCurrentState() {
        String output = "";
        for (int i = 0; i < this.currentSize; i++) {
            output = output + i + ": " + String.valueOf(
                    this.arrayInt[i]) + "\n";
        }
        return output;
    }
    /**
     * Main test.
     * @param args wow. oh wow.
     */
//    public static void main(String[] args) {
//        UnionFindQuickUnions test = new UnionFindQuickUnions(5);
//        System.out.println(test.getCurrentState());
//        test.union(1, 3);
//        
//        //System.out.println(test.find(3));
//        System.out.println(test.getCurrentState());
//        test.union(1,4);
//        System.out.println(test.getCurrentState());
//        test.union(4,0);
//        System.out.println(test.getCurrentState());
//        test.union(0,1);
//        System.out.println(test.getCurrentState());
//        test.union(3,4);
//        System.out.println(test.getCurrentState());
//        System.out.println(test.getNumSubsets());
//    }
}
