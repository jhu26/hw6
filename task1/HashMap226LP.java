//import java.lang.Object;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Hash Map Implemented with Linear Probing.
 * HashMap226LP.java
 * @author juliahu
 * @param <K> is type K
 * the key with which the specified value is to be associated
 * @param <V> is type V
 * value to be associated with the specified key (may be null)
 */
public class HashMap226LP<K, V> implements HashMap226<K, V> {
    /**
     * final int three = 3.
     */
    static final int THREE = 3;
    /**
     * initial capacity.
     */
    final int initialCapacity = 11;
    /**
     * initial load factor.
     */
    final double initialLoadFactor = 0.5;
    /**
     * maxloadfactor.
     */
    private double maxLoadFactor;
    /**
     * numbuckets.
     */
    private int numBuckets;
    /**
     * loadfactor.
     */
    private double loadFactor;
    /**
     * size.
     */
    private int size;
    /**
     * hashmap.
     */
    private MapEntry<K, V> [] hashMap;
    
    /**
     * constructor for HashMap226LP.
     * no arguments.
     */
    public HashMap226LP() {
        this.numBuckets = this.initialCapacity;
        this.hashMap = new MapEntry[this.numBuckets];
        this.maxLoadFactor = this.initialLoadFactor;
        this.size = 0;
        this.loadFactor = (double) (this.size) / (this.numBuckets);
    }
    
    /**
     * constructor for HashMap226LP.
     * two arguments initial capacity and load factor
     * @param initCapacity is initial capacity.
     * @param maxloadFactor is maxlaodfactor.
     */
    public HashMap226LP(int initCapacity, double maxloadFactor) {
        if (initCapacity < 0 || maxloadFactor <= 0) {
            throw new IllegalArgumentException("Arguments are incorrect.");
        } else {
            this.numBuckets = initCapacity;
            this.hashMap = new MapEntry[this.numBuckets];
            this.maxLoadFactor = (double) maxloadFactor;
            this.size = 0;
            this.loadFactor = (double) (this.size) / (this.numBuckets);
        }   
    }
    /**
     * Check if we need to rehash before put.
     * @return V value
     * @param key is key.
     * @param value is value.
     */  
    public V put(K key, V value) {
        //if key already exists
        if (this.containsKey(key)) {            
            return this.put(key, value, 0);
        } else { //if new key
            
            double tempLoadFactor = 
                    (double) (this.size + 1) / (this.numBuckets);

            //if loadFactor > maxloadfactor rehash first !
            if (tempLoadFactor > this.maxLoadFactor) {
                this.rehash();
                return this.put(key, value, 0);
            } else { // if its okay just put!
                return this.put(key, value, 0);
            }
            //update size and loadFactor
        }
    }
    
    /**
     * 
     * Associate the specified value with the specified key in this map.
     * If the map previously contained a mapping for the key, the old value 
     * is replaced.
     *
     * @param key
     *            the key with which the specified value is to be associated
     * @param value
     *            value to be associated with the specified key (may be null)
     * @param dummy is just a dummy to indicate which put function to use.
     * @return 
     *         the previous value associated with key, or null if there was
     *         no previous mapping for key.  (A null return value can also
     *         indicate that the map previously associated null with key.)
     */
    private V put(K key, V value, int dummy) {
        
        int position = Math.abs(key.hashCode()) % this.numBuckets;
        //while array is not empty and is zombie
        while (this.hashMap[position] != null) {
        //replace value if key is same
            if (this.hashMap[position].key == key) {
                V temp = (V) this.hashMap[position].value;
                this.hashMap[position].value = value;
                return temp;
            }
            //if zombie
            if (this.hashMap[position].isZombie) {
                this.hashMap[position].key = key;
                this.hashMap[position].value = value;
                this.size++;
                this.loadFactor = (double) (this.size / this.numBuckets);
                //no previous mapping
                return null;
            }
            position = (position + 1) % this.numBuckets;
        }
  
        //new entry
        this.hashMap[position] = new MapEntry<K, V>(key, value);
        this.size++;
        this.loadFactor = (double) (this.size) / (this.numBuckets);  
        //no previous mapping for key
        return null;
    }


    /**
     * Return the unique value to which the specified key is mapped, or null if
     * this map contains no mapping for the key.  (A return value of null does
     * not necessarily indicate that the map contains no mapping for the key, as
     * null values are permitted in this map.  Use containsKey to distinguish
     * the no-mapping-present case from the map-with-null-value case.)
     *
     * @param key
     *             the key whose associated value is to be returned
     * @return
     *          the value to which the specified key is mapped, or null if this
     *          map contains no mapping for the key
     */
    public V get(Object key) {
        int position = Math.abs(key.hashCode() % this.numBuckets);
        
        //while array is not empty and is zombie (will ignore zombies)
        while (this.hashMap[position] != null) {
            if (this.hashMap[position].key.equals(key)) {
                return (V) this.hashMap[position].value;
            }
            position = (position + 1) % this.numBuckets;
        }
        //doesn't exist
        return null;
    }

    /**
     * Return true if this map contains a mapping for the specified key.
     *
     * @param key
     *             the key whose presence in this map is to be tested
     * @return
     *             true if this map contains a mapping for the specified key
     */
    public boolean containsKey(Object key) {
        
        int position = Math.abs(key.hashCode() % this.numBuckets);
        
        //while true empties
        while (this.hashMap[position] != null) {
            if (this.hashMap[position].equals(key)) {
                return true;
            }
            position = (position + 1) % this.numBuckets;
        }
        return false;
    }
    /**
     * returns the key.
     * @param key is the key
     * @return K object
     */
    public K returnKey(K key) {
        int position = Math.abs(key.hashCode() % this.numBuckets);
   
        
        //while true empties
        while (this.hashMap[position] != null) {
            
            if ((this.hashMap[position].key).equals(key)) {
                
                return this.hashMap[position].key;
            }
            position = (position + 1) % this.numBuckets;
        }
        return null;
    }
    
    /**
     * rehash the array.
     */
  //size is same, load factor is changed!, num buckets changed 
    private void rehash() { 
        int tempNumBuckets = this.numBuckets; // current num buckets
        this.numBuckets = nextPrime(this.numBuckets * 2);
        this.size = 0;
        
        MapEntry<K, V>[] tempHashMap = new MapEntry[this.numBuckets];
        
        //for all elements in current hashmap
        for (int i = 0; i < tempNumBuckets; i++) {
            if (this.hashMap[i] == null) { 
                continue;
            }
            
            int position = Math.abs(this.hashMap[i].key.hashCode()) 
                    % this.numBuckets;
            
            if (this.hashMap != null) {  
                // if position open in temporary
                if (tempHashMap[position] == null) {
                    tempHashMap[position] = this.hashMap[i];
                    this.size++;
                } else { // position not open
                    while (tempHashMap[position] != null) {
                        position = (position + 1) % this.numBuckets;
                    }
                    tempHashMap[position] = this.hashMap[i];
                    this.size++;
                }
            }
            
        }
        this.hashMap = tempHashMap;
        this.loadFactor = (double) (this.size / this.numBuckets);
    }
    
    /**
     * adapted from Weiss textbook
     * Internal method to find a prime number at least as large as n.
     * @param n the starting number (must be positive).
     * @return a prime number larger than or equal to n.
     */
    private static int nextPrime(int n) {
        if (n % 2 == 0) {
            n++;
        }
        for (; !isPrime(n); n += 2) {
            @SuppressWarnings("unused")
            int p;
        }
        
        return n;
    }

    /**
     * adapted from Weiss textbook
     * Internal method to test if a number is prime.
     * Not an efficient algorithm.
     * @param n the number to test.
     * @return the result of the test.
     */
    private static boolean isPrime(int n) {
        if (n == 2 || n == THREE) {
            return true;
        }
        if (n == 1 || n % 2 == 0) {
            return false;
        }
        for (int i = THREE; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Remove the mapping for the specified key from this map if present.
     *
     * @param key
     *             the key whose mapping is to be removed from the map
     * @return
     *          the previous value associated with key, or null if there
     *          was no mapping for key.  (A null return can also indicate
     *          that the map previously associated null with the key.)
     */
    public V remove(Object key) {
        //mark isZombie
        int position = Math.abs(key.hashCode()) % this.numBuckets;
        
        //while true empties
        while (this.hashMap[position] != null) {
            if (this.hashMap[position].key == key) {
                V tempValue = this.hashMap[position].value;
                this.hashMap[position].key = null;
                this.hashMap[position].value = null;
                this.hashMap[position].isZombie = true;
                this.size--;
                return tempValue;
            }
            position = (position + 1) % this.numBuckets;
        }
        return null;
    }

    /**
     * Remove all of the mappings from this map.  The map will be empty
     * after this call returns.
     */
    public void clear() {
        //clear all
        for (int i = 0; i < this.numBuckets; i++) {
            this.hashMap[i] = null;
        }
        this.size = 0;
    }

    /**
     * Return the number of key-value mappings in this map.
     * 
     * @return
     *          the number of key-value mappings in this map
     */
    public int size() {
        return this.size;
    }
    
    /**
     * Return the current loadFactor.
     * 
     * @return
     *          current loadFactor.
     */
    public double numloadFactor() {
        return (double) (this.loadFactor);
    }
    
    /**
     * Return the number of buckets in hashmap.
     * 
     * @return
     *          the number of buckets in hashmap.
     */
    public int numberBuckets() {
        return this.numBuckets;
    }

    /**
     * Return a Set view of the keys contained in this map.  
     * 
     * @return
     *          a Set view of the keys contained in this map
     */
    public Set<K> keySet() {
        Set<K> hashKeys = new HashSet<K>();
        for (int i = 0; i < this.numBuckets; i++) {
            if (this.hashMap[i] != null) {
                if (this.hashMap[i].key != null) {
                    hashKeys.add(this.hashMap[i].key);
                }
            }
        }
        return hashKeys;       
    }

    /**
     * Return a Collection view of the values contained in this map. 
     * 
     * @return
     *          a Collection view of the values contained in this map
     */
    public Collection<V> values() {
        Collection<V> hashKeys = new ArrayList<V>();
        for (int i = 0; i < this.numBuckets; i++) {
            if (this.hashMap[i] != null) {
                if (this.hashMap[i].key != null) {
                    hashKeys.add(this.hashMap[i].value);
                }
            }
        }
        return hashKeys;
    }
    /**
     * private map class.
     * @author juliahu
     *
     * @param <K> key
     * @param <V> value
     */
    private class MapEntry<K, V> {
        /**
         * key is key.
         */
        K key;
        /**
         * value is value.
         */
        V value;
        /** 
         * is zombie.
         */
        boolean isZombie;
        /**
         * mapentry constructor.
         * @param key1 is key.
         * @param value1 is value.
         */
        public MapEntry(K key1, V value1) {
            this.key = key1;
            this.value = value1;
            this.isZombie = false;
        }

    }
}
