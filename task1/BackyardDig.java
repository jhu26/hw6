import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.Iterator;
import java.io.BufferedWriter;
import java.io.FileWriter;
/**
 * Backyard Dig.
 * @author juliahu
 *
 */
public class BackyardDig {

    /**
     * max size needed.
     */
    int maxSize;

    /**
     * Min Heap to look for shortest distances.
     */
    MinPQ<Edge> distancesHeap;
    
    /**
     * Array containing all edges and corresponding distances.
     */
    Edge[] list;
    
    /**
     * next avail spot of car list.
     */
    int index;
    
    /**
     * setName in hashmap;
     */
    int setName;
    
    /**
     * minimum distance.
     */
    int minDist;
    
    /**
     * union for nodes.
     */
    UnionFindQuickUnions unions;
    
    /**
     * hashmap to find name of the set nodes are in.
     */
    HashMap226LP<Car, Integer> hashmap;
    
    /**
     * array list final.
     */
    ArrayList<String> finalList;
    /**
     * backyard dig.
     * @param r is rows in grid.
     * @param c is cols in grid.
     */
    public BackyardDig(String r, String c) { 
        this.maxSize = this.calcSize(Integer.parseInt(r), Integer.parseInt(c));
        this.list = new Edge[this.maxSize];        
        this.hashmap = new HashMap226LP<Car, Integer>(this.maxSize, 0.5);
        this.finalList = new ArrayList<String>();
        this.index = 0;
        this.setName = 0;
        this.minDist = 0;
    }
    /**
     * calculates max number of edges with r*c vertices.
     * @param rows number of rows
     * @param cols number of cols
     * @return max size 
     */
    public int calcSize(int rows, int cols) {
        int size = rows * cols;
        int temp = 0;
        
        for (int i = size - 1; i > 0; i--) {
            temp = temp + i;
            
        }
        return temp + 1;
        
    }
    /**
     * add to array.
     * @param edge added to array and set
     *
     */
    public void addToArray(String[] edge) {
      
        Car car1 = new Car(edge[0]);
        Car car2 = new Car(edge[1]);
        Edge temp = new Edge(car1, car2, Integer.parseInt(edge[2])); 
       //add to array
        this.list[this.index++] = temp;
        
        //add to hashmap if not in it already
        if (!this.hashmap.containsKey(car1)) {
            this.hashmap.put(car1, this.setName);
            this.setName++;
        }
        if (!this.hashmap.containsKey(car2)) {
            this.hashmap.put(car2, this.setName);
            this.setName++;
        }      
    }
    //WRITE A RESIZE FUNCTION
    
    /**
     * add to heap.
     */
    public void addToHeap() {
        //add to heap
        this.distancesHeap = new MinPQ<Edge>(this.list, this.index);
        //add to union
        this.unions = new UnionFindQuickUnions(this.hashmap.size());
          
    }
    /**
     * kruska's algorithm
     */
    public int kruskalMST() {
       
        //while not empty
        while (!(this.distancesHeap.size() == 0)) {
            Edge temp = this.distancesHeap.delMin();
            int c1SetName = this.hashmap.get(temp.car1);
            int c2SetName = this.hashmap.get(temp.car2);
            
            //in same union
            if (this.unions.find(c1SetName) == this.unions.find(c2SetName)) {
                this.minDist = this.minDist + 0;
            } else {
              //add nodes to set               
                this.unions.union(c1SetName, c2SetName);                       
                this.minDist = this.minDist + temp.distance;
                this.finalList.add(temp.car1.getCar());
                this.finalList.add(temp.car2.getCar());
            }
        }
        return this.minDist;
    }
    /**
     * return final arraylist.
     * @return final arraylist.
     */
    public ArrayList<String> printFinal() {
        return this.finalList;
    }

    /**
     * main driver of backyard dig.
     * @param args input file name.
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        
        try {
            File file = new File(args[0]);
            Scanner sc = new Scanner(file);
            String input = sc.nextLine();
            String[] gridSize = input.trim().split(" ");
            
            //IF MORE THAN TWO IN ARRAY THROW SOMETHING           
            BackyardDig holes = new BackyardDig(gridSize[0], gridSize[1]);

            //while file not empty  
            while (sc.hasNextLine()) {
                String expression = sc.nextLine().trim();
                String[] edge = expression.split(" ");
                //add to heap
                holes.addToArray(edge);
                holes.addToHeap();      
            }    
            //check if file already exists.!!!! FIX THIS FIX THIS!
            File output = new File(args[1]);
            FileWriter fw = new FileWriter(output.getAbsoluteFile());
            BufferedWriter outputBuffer = new BufferedWriter(fw);
            
            //write to file
            outputBuffer.write(holes.kruskalMST() + "\n" + "\n");
            //System.out.println(holes.kruskalMST());
            ArrayList<String> finalOutput = holes.printFinal();
            //write to file
            Iterator<String> carIterator = finalOutput.iterator();
            while (carIterator.hasNext()) {
                //System.out.print(carIterator.next() + " ");
                outputBuffer.write(carIterator.next() + " ");
                outputBuffer.write(carIterator.next() + "\n");
                
                //System.out.println(carIterator.next());
                
            }

            outputBuffer.close();
 
            //find min distance
            
        } catch (FileNotFoundException e) {
            //what even is this.
            e.printStackTrace();
        }       
        
    }
}
