/*************************************************************************
 *  Compilation:  javac MinPQ.java
 *  Execution:    java MinPQ < input.txt
 *  
 *  Generic min priority queue implementation with a binary heap.
 *  Can be used with a comparator instead of the natural order.
 *
 *  % java MinPQ < tinyPQ.txt
 *  E A E (6 left on pq)
 *
 *  We use a one-based array to simplify parent and child calculations.
 *
 *  Can be optimized by replacing full exchanges with half exchanges
 *  (ala insertion sort).
 *  adapted from Robert Sedgewick and Kevin Wayne.
 *************************************************************************/

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 *  The <tt>MinPQ</tt> class represents a priority queue of generic keys.
 *  It supports the usual <em>insert</em> and <em>delete-the-minimum</em>
 *  operations, along with methods for peeking at the minimum key,
 *  testing if the priority queue is empty, and iterating through
 *  the keys.
 *  <p>
 *  This implementation uses a binary heap.
 *  The <em>insert</em> and <em>delete-the-minimum</em> operations take
 *  logarithmic amortized time.
 *  items used to initialize the data structure.
 *  <p>
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
/**
 * minpq class.
 * @author juliahu
 *
 * @param <Key>
 */
public class MinPQ<Key> implements Iterable<Key> {
    /**
     * final int four = 4.
     */
    static final int FOUR = 4;
    /**
     * pq key[].
     */
    private Key[] pq;                    // store items at indices 1 to N
    /**
     * int N.
     */
 // number of items on priority queue
    private int maxSize;
    /**
     * comparator.
     */
    private Comparator<Key> comparator; 

    /**
     * Initializes an empty priority queue with the given initial capacity.
     * @param initCapacity the initial capacity of the priority queue
     */
    public MinPQ(int initCapacity) {
        pq = (Key[]) new Object[initCapacity + 1];
        this.maxSize = 0;
    }

    /**
     * Initializes an empty priority queue.
     */
    public MinPQ() {
        this(1);
    }

    /**
     * Initializes an empty priority queue with the given initial capacity,
     * using the given comparator.
     * @param initCapacity the initial capacity of the priority queue
     * @param comparator1 the order to use when comparing keys
     */
    public MinPQ(int initCapacity, Comparator<Key> comparator1) {
        this.comparator = comparator1;
        this.pq = (Key[]) new Object[initCapacity + 1];
        this.maxSize = 0;
    }

    /**
     * Initializes an empty priority queue using the given comparator.
     * @param comparator1 the order to use when comparing keys
     */
    public MinPQ(Comparator<Key> comparator1) { 
        this(1, comparator1); 
    }

    /**
     * Initializes a priority queue from the array of keys.
     * Takes time proportional to the number of keys, 
     * using sink-based heap construction.
     * @param keys the array of keys
     * @param size of array.
     */
    public MinPQ(Key[] keys, int size) {
        this.maxSize = size;
        
        this.pq = (Key[]) new Object[keys.length + 1];
        for (int i = 0; i < this.maxSize; i++) {
            this.pq[i + 1] = keys[i];
        }
        for (int k = this.maxSize / 2; k >= 1; k--) {
            this.sink(k);
        }
        assert this.isMinHeap();
    }
    
    
    /**
     * Is the priority queue empty?
     * @return true if the priority queue is empty; false otherwise
     */
    public boolean isEmpty() {
        return this.maxSize == 0;
    }

    /**
     * Returns the number of keys on the priority queue.
     * @return the number of keys on the priority queue
     */
    public int size() {
        return this.maxSize;
    }

    /**
     * Returns a smallest key on the priority queue.
     * @return a smallest key on the priority queue
     * @throws java.util.NoSuchElementException if priority queue is empty
     */
    public Key min() {
        if (this.isEmpty()) {
            throw new NoSuchElementException(
                    "Priority queue underflow");
        }
        return this.pq[1];
    }

    // helper function to double the size of the heap array
    /**
     * resize minpq.
     * @param capacity is capacity.
     */
    private void resize(int capacity) {
        assert capacity > this.maxSize;
        Key[] temp = (Key[]) new Object[capacity];
        for (int i = 1; i <= this.maxSize; i++) {
            temp[i] = this.pq[i];
        }
        this.pq = temp;
    }

    /**
     * Adds a new key to the priority queue.
     * @param x the key to add to the priority queue
     * @return int
     */
    public int insert(Key x) {
        // double size of array if necessary
        if (this.maxSize == this.pq.length - 1) {
            this.resize(
                    2 * this.pq.length);
        }

        // add x, and percolate it up to maintain heap invariant
        this.pq[++this.maxSize] = x;
        int position = this.swim(this.maxSize);
        //System.out.println("swimposition" + position);
        assert this.isMinHeap();
        return position;
    }

    /**
     * Removes and returns a smallest key on the priority queue.
     * @return a smallest key on the priority queue
     * @throws java.util.NoSuchElementException if the priority queue is empty
     */
    public Key delMin() {
        if (this.isEmpty()) {
            throw new NoSuchElementException("Priority queue underflow");
        }
        this.exch(1, this.maxSize);
        Key min = this.pq[this.maxSize--];
        this.sink(1);
        this.pq[this.maxSize + 1] = null;
        if ((this.maxSize > 0) && (this.maxSize == (
                this.pq.length - 1) / FOUR)) {
            this.resize(this.pq.length  / 2);
        }
        assert this.isMinHeap();
        return min;
    }


   /***********************************************************************
    * Helper functions to restore the heap invariant.
    **********************************************************************/
    /**
     * swim function.
     * @param k integer
     * @return integer
     */
    private int swim(int k) {
        while (k > 1 && this.greater(k / 2, k)) {
            this.exch(k, k / 2);
            k = k / 2;
        }
        return k;
    }
    /**
     * sink function.
     * @param k is integer
     * @return is integer
     */
    private int sink(int k) {
        while (2 * k <= this.maxSize) {
            int j = 2 * k;
            if (j < this.maxSize && this.greater(j, j + 1)) {
                j++;
            }
            if (!this.greater(k, j)) {
                break;
            }
            this.exch(k, j);
            k = j;
        }
        return k;
    }

   /***********************************************************************
    * Helper functions for compares and swaps.
    **********************************************************************/
    /**
     * great function.
     * @param i is i.
     * @param j is j.
     * @return boolean.
     */
    private boolean greater(int i, int j) {
        if (this.comparator == null) {
            return ((Comparable<Key>) this.pq[i]).compareTo(this.pq[j]) > 0;
        } else {
            return this.comparator.compare(this.pq[i], this.pq[j]) > 0;
        }
    }
    
    /**
     * exhange function.
     * @param i is i.
     * @param j is j.
     */
    private void exch(int i, int j) {
        Key swap = this.pq[i];
        this.pq[i] = this.pq[j];
        this.pq[j] = swap;
    }

    // is pq[1..N] a min heap?
    /**
     * is min heap.
     * @return boolean.
     */
    private boolean isMinHeap() {
        return this.isMinHeap(1);
    }

    // is subtree of pq[1..N] rooted at k a min heap?
    /**
     * min heap.
     * @param k is k.
     * @return is boolean.
     */
    private boolean isMinHeap(int k) {
        if (k > this.maxSize) {
            return true;
        }
        int left = 2 * k, right = 2 * k + 1;
        if (left  <= this.maxSize && this.greater(k, left)) {
            return false;
        }
        if (right <= this.maxSize && this.greater(k, right)) {
            return false;
        }
        return this.isMinHeap(left) && this.isMinHeap(right);
    }

   /***********************************************************************
    * Iterators
    **********************************************************************/

    /**
     * Returns an iterator that iterates over the keys on the priority queue
     * in ascending order.
     * The iterator doesn't implement <tt>remove()</tt> since it's optional.
     * @return an iterator that iterates over the keys in ascending order
     */
    public Iterator<Key> iterator() { 
        return new HeapIterator(); 
    }
    /**
     * heap iterator.
     */
    private class HeapIterator implements Iterator<Key> {
        // create a new pq
        /**
         * euweh.
         */
        private MinPQ<Key> copy;

        // add all items to copy of heap
        // takes linear time since already in heap order so no keys move
        /**
         * it is what it is.
         */
        public HeapIterator() {
            if (MinPQ.this.comparator == null) {
                this.copy = new MinPQ<Key>(MinPQ.this.size());
            } else {
                this.copy = new MinPQ<Key>(
                        MinPQ.this.size(), MinPQ.this.comparator);
            }
            for (int i = 1; i <= MinPQ.this.maxSize; i++) {
                this.copy.insert(MinPQ.this.pq[i]);
            }
        }

        /**
         * HI.
         * @return :)
         */
        public boolean hasNext() {  
            return !this.copy.isEmpty();
        }
        
        /**
         * do you actually read this?
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }

        /**
         * heh.
         * @return hi
         */
        public Key next() {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            return this.copy.delMin();
        }
    }


}