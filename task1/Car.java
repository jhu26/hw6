/**
 * Car class. (aka Node)
 * @author juliahu
 *
 */
public class Car implements Comparable<Car> {
    /**
     * name of car.
     */
    private String car;
    
    
    /**
     * Car constructor.
     * @param c for car.
     */
    public Car(String c) {
        this.car = c;
    }
    /**
     * get car name.
     * @return car name
     */
    public String getCar() {
        return this.car;
    }
    
    @Override
    public int compareTo(Car otherItem) {
        if (this.car == otherItem.car) {
            return 0;
        } else if (this.car.compareTo(otherItem.car) > 0) {
            return 1;
        } else if (this.car.compareTo(otherItem.car) < 0) {
            return -1;
        } else {
            return 0;
        }
    }
    
    @Override
    public int hashCode() {
        return this.car.hashCode();
    }

    @Override
    public boolean equals(Object otherItem) {
      
        if (otherItem instanceof Car) {
            Car temp = (Car) otherItem;
            if (this.car.compareTo(temp.car) == 0) {                
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
