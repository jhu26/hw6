===================================
Task 2: CodeSort Readme
===================================

After reading the Task 2 requirement, we immediately thought about using a 
topological sort. We want to use a topological sort because from the 
smallSorted.txt file, we can get many pairs of relative orderings for characters.
We can easily represent and the these relative/partial orderings with a directed 
graph. And after we have the graph, the most efficient ways to get an absolute 
ordering for these characters is to use a topological sort. Once we have an
absolute ordering, we can sort the unsorted.txt file using a sort method and output
the desired file.

Graph Class:

First, to store all the characters in a graph, we wrote a graph class. Inside the 
graph class, there are a vertex class and a edge class. The graph class has an ArrayList
of vertices. The reason why we chose to use an ArrayList is because it’s the best fit for
our program. A HashSet can be a choice for other teams. However, for our program, we want 
to traverse through the list and get the actual vertices when we need them. A HashSet 
will be able to give us better running time overall but it wouldn’t be able to return the 
actual vertex and the position of it when we need it. In addition, we also have two int
numv, which keeps track of the number of vertices and nume, which keeps track of the number
of edges.There are a method to get the vlist, a checker method to print everything
in the vlist, a method to add vertex, a method to add edge, a method to return numv,
a method to return nume, a contains vertex method, a delete vertex method, a delete
edge method.

Vertex Class:

In our vertex class, we have three data fields: one ArrayList of incoming edges, one 
ArrayList of outgoing edges and a String name. we have an add incoming edge method,
an add outgoing edge method, a get name method, a delete outgoing edge method, a
delete incoming edge method, a get ArrayList incoming method, a get ArrayList outgoinging 
method, a delete all outgoing edges method.

Edge Class:

In our edge class, we have two data fields: one Vertex from and one vertex to. We have
a method to get the vertex from and a method to get the vertex to.


CodeSort Class:

In our CodeSort class, we have an ArrayList of string, sortedwords, which stores all
the words we get from our first file, an ArrayList of string, unsortedwords, which has 
all the unsorted words we get from the second file and which we sort later, an array of
string sorted, which stores the ordered characters, a graph g, a HashMap<String, Integer>
h which we store the characters with the priority value into. The reason why we use a
HashMap is because we don’t care about order of things (etc. LinkedList, ArrayList, but we
care about the easy access of value by key.

In our main, we first read in the sorted words into sorted words and unsorted words into
unsorted words, then we store all the characters into a directed graph g. Afterwards, we
do a topological sort to figure out the absolute order of characters and store the order
in an array list, which we then store into a HashMap h. Then, we conduct a bubble sort on
the unsortedwords list. After we are done with that, we just output the sorted list to the
third file.

Note:

Technically, we don’t need the two ArrayList, as we don’t have to store all the sorted 
characters and we can just process all the characters from and store them into the 
HashMap. However, to break up the methods and make the code easily understandable. 
we decided to include the two arraylists.
