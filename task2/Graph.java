import java.util.ArrayList;

/**
 * Graph class.
 */
public class Graph {
    
    /**
     * ArrayList of vertex.
     */
    ArrayList<Vertex> vlist;
    
    /**
     * Number of Vertex/ Vertices.
     */
    private int numv;
    
    /**
     * Number of Edge/ Edges.
     */
    private int nume;
    
    /**
     * Constructor of Graph.
     */
    public Graph() {
        this.vlist = new ArrayList<Vertex>();
        this.numv = 0;
        this.nume = 0;       
    }
    
    /**
     * vertex list getter.
     * @return ArrayList<Vertex>
     */
    public ArrayList<Vertex> getvlist() {
        return this.vlist;
    }
    
    /**
     * Checker method.
     */
    public void printv() {
        for (int i = 0; i < this.vlist.size(); i++) {
            System.out.println(this.vlist.get(i).name);
        }
    }

    /**
     * Add Vertex.
     * @param v Vertex v;
     */
    public void addV(String v) {
        this.numv++;
        Vertex nv = new Vertex(v);
        this.vlist.add(nv);        
    }
    
    /**
     * Add Edge.
     * @param f String from
     * @param t String To
     * 
     */
    public void addE(String f, String t) {
        this.nume++;
        int fposition = 0;
        int tposition = 0;
        for (int i = 0; i < this.vlist.size(); i++) {
            if (this.vlist.get(i).getName().equals(f)) {
                fposition = i;
            }
        }
        for (int i = 0; i < this.vlist.size(); i++) {
            if (this.vlist.get(i).getName().equals(t)) {
                tposition = i;
            }
        }
        Edge ne = new Edge(this.vlist.get(fposition), 
                this.vlist.get(tposition));   
        
        this.vlist.get(fposition).addOutgoing(ne);
        this.vlist.get(tposition).addIncoming(ne);
    }
    /**
     * @return integer number of V.
     */
    public int numv() {
        return this.numv;
    }
    
    /**
     * @return integer number of E.
     * 
     */
    public int nume() {
        return this.nume;
    }
    
    /**
     * contains method.
     * @param v String v.
     * @return boolean true or false.
     * 
     */
    public boolean contains(String v) {
        for (int i = 0; i < this.vlist.size(); i++) {
            if (this.vlist.get(i).getName().equals(v)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * delete vertex method.
     * @param v Vertex v.
     * 
     */
    public void deleteV(Vertex v) {
        this.numv--;
        this.deleteE(v);
        this.vlist.remove(v);       
    }  
    
    /**
     * delete outgoing edges of Vertex vname.
     * @param vname String v.
     * 
     */
    public void deleteE(Vertex vname) {
        int outsize = vname.getOutgoing().size();
        
        for (int i = outsize - 1; i >= 0; i--) {
            vname.deleteOutEdge(vname.getOutgoing().get(i).to); //          
        } 
        vname.clearoutgoing();    
    }
    
    /**
     * Vertex class.
     */
    protected class Vertex {
        
        /**
         * Vertex name.
         */
        String name;
        
        /**
         * Incoming Edges.
         */
        ArrayList<Edge> incoming;
        
        /**
         * Outgoing Edges.
         */
        ArrayList<Edge> outgoing;
             
        /**
         * Vertex constructor.
         * @param n String name.
         */
        Vertex(String n) {
            this.name = n;
            this.incoming = new ArrayList<Edge>();
            this.outgoing = new ArrayList<Edge>();
        }
        /**
         * Add incoming edge.
         * @param e edge
         */
        public void addIncoming(Edge e) {
            this.incoming.add(e);
        }
        /**
         * Add outgoing edge.
         * @param e edge
         */
        public void addOutgoing(Edge e) {
            this.outgoing.add(e);
        }
        
        /**
         * Add outgoing edge.
         * @return String Name
         */
        public String getName() {
            return this.name;
        }
        
        /**
         * Remove an edge from the current vertex to t.
         * @param t vertex to.
         */
        public void deleteOutEdge(Vertex t) {
            for (int i = 0; i < this.outgoing.size(); i++) {
                if (this.outgoing.get(i).to.getName().equals(t.getName())) {
                    this.outgoing.remove(i);
                    t.deleteInEdge(this);
                    return;
                }
            }           
        }

        /**
         * Remove an edge from the f to the current vertex.
         * @param f vertex from.
         */
        public void deleteInEdge(Vertex f) {
            for (int i = 0; i < this.incoming.size(); i++) {
                if (this.incoming.get(i).to.getName().equals(this.name)) {
                    this.incoming.remove(i);
                    return;
                }
            }           
        }     
        
        /**
         * Getter for array incoming.
         * @return ArrayList<Edge> incoming
         */
        public ArrayList<Edge> getIncoming() {
            return this.incoming;
        }
        
        /**
         * Getter for array outgoing.
         * @return ArrayList<Edge> incoming
         */
        public ArrayList<Edge> getOutgoing() {
            return this.outgoing;
        }
        
        /**
         * Clear Outgoing ArrayList.
         */
        public void clearoutgoing() {
            ArrayList<Edge> newlist = new ArrayList<Edge>();
            this.outgoing = newlist;
        }
        
    }
    
    /**
     * Edge class.
     */
    protected class Edge {

        /**
         * vertex from.
         */
        Vertex from;
        /**
         * vertex to.
         */
        Vertex to;
        
        /**
         * Edge constructor.
         * @param f Vertex from
         * @param t Vertex To
         */
        Edge(Vertex f, Vertex t) {
            this.from = f;
            this.to = t;
        }
        
        /**
         * Vertex from getter.
         * @return Vertex from
         */
        public Vertex getFrom() {
            return this.from;
        }
        
        /**
         * Vertex to getter.
         * @return Vertex to
         */
        public Vertex getTo() {
            return this.to;
        }
        
        
    }
}
