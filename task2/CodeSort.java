import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;

/**
 * Process class.
 */
public final class CodeSort {
    
    /**
     * ArrayList that stores words.
     */
    private static ArrayList<String> sortedwords = new ArrayList<String>();
    
    /**
     * ArrayList that stores the unsorted words.
     */
    private static ArrayList<String> unsortedwords = new ArrayList<String>();
    
    /**
     * Array that stores the ordered characters.
     */
    private static String [] sortedc;
    
    /**
     * New graph.
     */
    private static Graph g = new Graph();
    
    /**
     * New graph.
     */
    private static HashMap<String, Integer> h = new HashMap<String, Integer>();
    
    /**
     * Constructor.
     */
    private CodeSort() {     
    }

    /**
     * Main method.
     * @param args string arguments.
     */
    public static void main(String[] args) {

        File shortsorted = new File(args[0]);
        File unsorted = new File(args[1]);
        String sorted = args[2];
        
        try {
            Scanner sc1 = new Scanner(shortsorted);
            while (sc1.hasNextLine()) {
                sortedwords.add(sc1.nextLine());
            }
            sc1.close();
        } catch (FileNotFoundException e) {
            System.out.println("Sorted File does not exist");
        }
        
        try {
            Scanner sc2 = new Scanner(unsorted);
            while (sc2.hasNextLine()) {
                unsortedwords.add(sc2.nextLine());
            }
            sc2.close();
        } catch (FileNotFoundException e) {
            System.out.println("Unsorted File does not exist");
        }
        
        store();   
        toposort();
        process();       
        bubblesort();
        
        try {
            PrintWriter pw = new PrintWriter(sorted);
            for (int i = 0; i < unsortedwords.size(); i++) {
                pw.println(unsortedwords.get(i));
            }
            pw.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
    }
    /**
     * Read in the sorted short file.
     */

    public static void store() {
        if (sortedwords.size() < 1) {
            System.out.println("Empty inputting file");
        } else {
            for (int i = 1; i < sortedwords.size(); i++) {
                
                String word1 = (String) sortedwords.get(i - 1);
                String word2 = (String) sortedwords.get(i);
                
                if (word1.compareTo(word2) != 0) { //
                    int j = 0;
                    while (word1.charAt(j) == word2.charAt(j)) {
                        j++;
                    }                  
                    
                    // what if ??; ? case
                    //if there is at least one empty space
                    if ((String.valueOf(word1.charAt(j)).equals(" "))
                        || (String.valueOf(word2.charAt(j)).equals(" "))) {
                        continue;
                    }
                    
                    String v1 = word1.charAt(j) + "";
                    //if Vertex f doesn't exist yet
                    if (!g.contains(v1)) {
                        g.addV(v1);
                    }
                 
                    String v2 = word2.charAt(j) + "";
                    //if Vertex t doesn't exist yet
                    if (!g.contains(v2)) {
                        g.addV(v2);
                    }  
                    
                    //add an edge between the two vertices.
                    g.addE(v1, v2);
                }
            }
        }
      
    }
    
    /**
     * Topological sort.
     */
// maybe in graph?
    public static void toposort() {
        
        sortedc = new String [g.getvlist().size()];
        int position = 0;
        while (g.getvlist().size() > 0) {
            for (int i = 0; i < g.getvlist().size(); i++) {
                if (g.getvlist().get(i).getIncoming().size() == 0) {
                    sortedc[position] = g.getvlist().get(i).getName();
                    position++;
                    g.deleteV(g.getvlist().get(i));
                }
            }
        }
    }
    
    /**
     * Process everything into HashMap.
     */
    public static void process() {
        for (int i = 0; i < sortedc.length; i++) {
            h.put(sortedc[i], i);
        }
    }
    
    
    /**
     * Bubble sort.
     * @author Joanne
     */
    public static void bubblesort() {
        String temp;
        for (int size = unsortedwords.size() - 1; size >= 1; size--) {          
            for (int i = 0; i < size; i++) {
                if (compare(unsortedwords.get(i), 
                        unsortedwords.get(i + 1)) > 0) {
                    temp = unsortedwords.get(i);
                    unsortedwords.set(i, unsortedwords.get(i + 1));
                    unsortedwords.set((i + 1), temp);
                }
            }
        }
        
    }
    /**
     * Compare.
     * @param x String x
     * @param y String y
     * @return integer return 0 for equal, 1 for x > y and -1 for x < y.
     */
    public static int compare(String x, String y) {
        
        if (x.compareTo(y) == 0) { 
            //the two strings are the exact same
            return 0;
        } else {
            int j = 0;
            while (x.charAt(j) == y.charAt(j)) {
                j++;
            }
            int xvalue = h.get(x.charAt(j) + "");
            int yvalue = h.get(y.charAt(j) + "");
            
            if (xvalue > yvalue) {
                return 1;
            } else {
                return -1;
            }
        }
        
    }

}
