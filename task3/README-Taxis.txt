========================================
Task 3:
========================================

* compile:
make

* run: 
java Taxi 3 mapLocations.txt mapConnections.txt driverLocations.txt 
java Taxi 3 mapLocations2.txt mapConnections2.txt driverLocations2.txt 

Description:
Note: Many things are key'd on the location name. I avoid 
using a vertex class as the key because I change the info stored within the vertex
class.

I represent my undirected graph within my Graph class 
with an adjacency list--implemented with a
hashmap with the key as string (location name) and value as a linked list of 
vertices (Loc class). In addition, my graph class is also implemented
with a hashmap with key as the location name but maps to the exact
Loc class as the value.

I implement the weights of my graph in a class called Traffic, which
is implemented with a hashmap of the location pair represented as a String
and the value being the distance. This "weight function" is used in
my dijkstra.

I also keep track of my drivers in the similar way--a hashmap key'd
on the location name and value being a linkedlist of Driver classes. 

Now, to solve the problem I first form my graph and initialize the necessary variables.
Then, I run dijkstra and solve the single source shortest paths problem
with the source being the pickup location. All nodes are updated with 
a parent pointer that will lead back to the source. Furthermore, they are
updated with the min-distance field, which stores the minimum distance (travel time)
from that location. The weights are gotten from the Traffic class in O(1).

After the dijkstra run, all the heavy lifting is done. 
To find the k nearest drivers, I sort my locations on the minium distance
field. Then, I loop through each location, grabbing drivers until I get k
drivers.

From the location of the selected driver, I simply follow the parent pointers
back to the source and there is my route. 