/*
 * Michael Gao
 * Hw 5 Task 3: Taxi
 */
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.Scanner;

/** taxi. */
public final class Taxi {
    /** TWO. */
    private static final int TWO = 2;
    /** THREE. */
    private static final int THREE = 3;  
    /** four. */
    private static final int FOUR = 4; 

    /** happy now checkstyle. */
    private Taxi() {
    }

    /**
     * main.
     * @param args them args
     */
    public static void main(String[] args) {
        if (args.length != FOUR) {
            System.out.print("Usage: [k] [map locations f] ");
            System.out.print("[map connections f] [driver locations f]\n");
            System.exit(1);
        }
        int k = Integer.parseInt(args[0]);
        File locF = new File(args[1]);
        File conF = new File(args[TWO]);
        File drvF = new File(args[THREE]);
        

        //0. init Everything
        Graph   graph   = new Graph();
        Traffic traffic = new Traffic();
        Drivers drivers = new Drivers();

        setup(locF, conF, drvF, graph, traffic, drivers);
        
        //4. print list of locations followed by ID.
        //   can store this in hashmap key'ed on ID
        String[] listLoc = graph.getListLoc();
        System.out.println(" ==========================");
        System.out.println(" === * Map Locations! * ===");
        int i2;
        for (int i = 0; i < listLoc.length; i++) {
            i2 = i + 1; //subtract later
            System.out.println("\t" + i2 + ": " + listLoc[i]);
        }

        Scanner inputScan = new Scanner(System.in);
        String input = "";
        Integer inputInt = -1;
        //a little error checking for alphanumeric stuff
        do {
            System.out.println("Where are ya? Enter the number: ");
            input = inputScan.nextLine();
            if (input == null) {
                continue;
            } else {
                input = input.replaceAll("[^0-9]", "");
                inputInt = Integer.parseInt(input);
            }
        } while (inputInt < 1 || inputInt > listLoc.length);

        String source = listLoc[inputInt - 1];
        System.out.println("\nChosen location: " + source);
        System.out.println("=======================================");
        System.out.println("=== * The " + k + " taxis closest to you! * ===");
        

        //5. Single Source Shortest Path problem
        //   Run Dijkstra from selected loc
        //   also storing parent pointers and 
        //   shortest distance at each node to selected location 
        dijkstra(graph, traffic, graph.getV(source));
        Loc[] listLocClass = graph.getListLocClass();
        Arrays.sort(listLocClass);

        //6. Sort all locations (nodes) based on minimum distance 
        //   from the source and print the first k
        //   All the while it will update distances 
        //   stored within Driver class and
        //   create a hashmap size k 
        //   between Driver's ID and its location
        HashMap<Integer, String> driversMapID2Loc = 
                    new HashMap<Integer, String>();
        getkDrivers(driversMapID2Loc, drivers, 
                                listLocClass, k);
        
        //get ID input
        input = "";
        Integer chosenDriver = null;
        //a little error checking for alphanumeric stuff
        do {
            System.out.println("Pick a taxi ID! ");
            input = inputScan.nextLine();
            if (input == null) {
                continue;
            } else {
                input = input.replaceAll("[^0-9]", "");
                chosenDriver = Integer.parseInt(input);
            }
        } while (!driversMapID2Loc.containsKey(chosenDriver));
        String chosenDriverLoc = driversMapID2Loc.get(chosenDriver);

        //7. Follows the parent node pointers from k back to location
        //   For travel time, use the name of location of the driver 
        //   to get it from g.access
        //   print Start: the location of driver
        //   print End: last element of the array, if size >= 1;
        ArrayList<String> route = getRoute(graph, chosenDriverLoc);
        int eta = graph.getV(chosenDriverLoc).getD();
        printRoute(route, chosenDriver, chosenDriverLoc, source, eta);
        return;
    }



    /** 
     * Setup hey hey.
     * @param locF    File for location
     * @param conF    File for connections
     * @param drvF    File for drivers
     * @param graph   the graph
     * @param drivers hashmap for location and drivers
     * @param traffic hashmap for edge weights.
     */
    static void setup(File locF, File conF, File drvF, 
               Graph graph, Traffic traffic, Drivers drivers) {
        try {
            BufferedReader rlocF = new BufferedReader(new FileReader(locF));
            BufferedReader rconF = new BufferedReader(new FileReader(conF));
            BufferedReader rdrvF = new BufferedReader(new FileReader(drvF));
            String line;

        //1. setup: get map locations stored in 
        //          A. Graph as key.
        //          B. Drivers as key. (all of'em, no driver->0)
        //          C. print # locations
            int count = 0;
            System.out.print("\nReading " + locF + " ... ");
            while ((line = rlocF.readLine()) != null) {
                String name = line.trim();  
                graph.addV(name, new Loc(name));
                drivers.addLocation(name);
                count++;
            }
            System.out.print(count + " locations.\n");

        //2. setup: enter in map connections in 
        //          A. Traffic (key,v) = (string ordered pair, int dist)
        //          B. Graph values: (Loc classes from a->b and b->a in)
        //          C. print # of connections.
            count = 0;
            String[] epts = new String[TWO]; 
            Integer wt = null;
            System.out.print("Reading " + conF + " ... ");
            while ((line = rconF.readLine()) != null) {  
                String[] linesplit = line.split("[ ]{2,}|\t+");
                wt = Integer.parseInt(linesplit[1]);
                epts = traffic.addConnection(linesplit[0], wt);
                graph.addE(graph.getV(epts[0]), graph.getV(epts[1]));
                count++;
            }
            System.out.print(count + " connections.\n");

        //3. setup: enter driver locations into Driver map
        //          print # of drivers.
            count = 0;
            Integer id = null;
            System.out.print("Reading " + drvF + " ... ");
            while ((line = rdrvF.readLine()) != null) {  
                //split only on two or more spaces
                String[] drvline = line.split("[ ]{2,}|\t+");
                id = Integer.parseInt(drvline[0].trim());
                drivers.addDriver(id, drvline[1].trim());
                count++;
            }
            System.out.print(count + " drivers.\n\n");
        } catch (IOException e) {
            System.out.println("Uh oh. Setup error.");
            System.exit(1);
        }
    }

    /** 
     * get k drivers.
     * @param driversMapID2Loc hashmap to get from driver ID to loc
     * @param drivers          drivers class.
     * @param listLocClass     array of Loc classes, sorted on minD
     * @param k                the number of taxis
     */
    static void getkDrivers(HashMap<Integer, String> driversMapID2Loc, 
                            Drivers drivers, Loc[] listLocClass, int k) {
        int kDriversFound = 0;
        for (int i = 0; i < listLocClass.length; i++) {
            String names = listLocClass[i].getName();
            if (drivers.hasDriver(names)) {
                Iterator<Driver> 
                    iterDr = drivers.getDriversIter(names);
                while (iterDr.hasNext()) {
                    Driver tmpd = iterDr.next();
                    System.out.print("\tDriver ID: " + tmpd.getId());
                    System.out.print(" is at " + tmpd.whereRU() + "\n");
                    tmpd.setDist(listLocClass[i].getD());
                    driversMapID2Loc.put(tmpd.getId(), tmpd.whereRU());
                    kDriversFound++;
                    if (kDriversFound == k) {
                        break;
                    }
                }
            }
            if (kDriversFound == k) {
                break;
            }
        }
        return;
    }

    /**
     * print the route.
     * @param route           route array of locations.
     * @param chosenDriver    int for id of chosen driver
     * @param chosenDriverLoc self explanatory.
     * @param source          pickup loc.
     * @param eta             time it takes to get you.
     */
    static void printRoute(ArrayList<String> route, int chosenDriver, 
                           String chosenDriverLoc, String source, int eta) {
        System.out.println("\nThe route taxi " + chosenDriver 
                            + " should drive to " + source + "\n");
        System.out.println("\t* Start: " + chosenDriverLoc);
        // System.out.println("\t* Route: " + route.size());
        if (route.size() == 1) {
            System.out.println("\t  " + chosenDriverLoc + " -> " + source);
        }
        for (int i = 1; i < route.size(); i++) {
            if (i == 1) {
                System.out.println("\t  " + chosenDriverLoc 
                                    + " -> " + route.get(i - 1));
            }
            System.out.println("\t  " + route.get(i - 1) 
                                + " -> " + route.get(i));
        }
        System.out.println("\t* End: " + source);
        System.out.println("\nEst. Travel Time with Traffic: " 
                            + eta + " minutes");
        
        System.out.println("\n \n=== * Good Bye! * ===");
        System.out.println("=====================");
        return;
    }

    /**
     * Dijkstra on selected Location.
     * @param g the graph
     * @param w Traffic class for the weights.
     * @param s Loc class for the starting source (for pickup)
     * @return  UNSORTED ArrayList of the Locs 
     *          that have the true minD to s in them
     */
    private static ArrayList<Loc> dijkstra(Graph g, Traffic w, Loc s) {
        ArrayList<Loc> done = new ArrayList<Loc>();

        PriorityQueue<Loc> q = new PriorityQueue<Loc>();
        Loc u = null;
        Iterator<Loc> iter = null;

        s.setD(0);
        q.add(s);
        while (q.size() != 0) {
            u = q.poll();
            done.add(u);
            iter = g.getNeighbors(u.getName());
            while (iter.hasNext()) {
                //RELAXATION
                Loc v = iter.next();
                int edgeWeight = w.getTime(u.getName(), v.getName());
                if (v.getD() > u.getD() + edgeWeight) {
                    // System.out.println("relaxed");
                    //just in case need to update, must reinsert
                    q.remove(v); 
                    v.setD(u.getD() + edgeWeight);
                    v.setP(u);
                    q.add(v); //reinsert
                }
            }
        }
        return done;
    }

    /**
     * get route. Called after dijkstra.
     * Follow parent pointer back to source.
     * @param  g the graph
     * @param  driverLoc string of the location
     * @return  string array of ordered pairs
     */
    static ArrayList<String> getRoute(Graph g, String driverLoc) {
        ArrayList<String> out = new ArrayList<String>();
        Loc from = g.getV(driverLoc);
        while (from.getP() != null) {
            out.add(from.getP().getName());
            from = from.getP();
        }
        return out;
    }

    /**
     * my graph as an Adj list.
     * 
     */
    private static class Graph {
        /** map for adj list. */
        Map<String, LinkedList<Loc>> adjl;
        /** map for name to Loc class. */
        Map<String, Loc> access;
        /** num Locs. */
        int nV;
        /** num edges. */
        int nE;

        /** def. constr. */
        Graph() {
            this.adjl = new HashMap<String, LinkedList<Loc>>();
            this.access = new HashMap<String, Loc>();
            this.nV = 0;
            this.nE = 0;
        }

        /** 
         * add Vertex.
         * @param lname String name of location.
         * @param l the location class;
         */
        void addV(String lname, Loc l) {
            this.adjl.put(lname, new LinkedList<Loc>());
            this.access.put(lname, l);
            this.nV++;
        } 

        /** 
         * add edge (undirected).
         * @param v1 first vertex
         * @param v2 second vertex
         */
        void addE(Loc v1, Loc v2) {
            this.adjl.get(v1.getName()).add(v2);
            this.adjl.get(v2.getName()).add(v1);
            this.nE++;
        }

        /**
         * get vertex.
         * @param  lname name to find and return loc.
         * @return Loc class.
         */
        Loc getV(String lname) {
            return this.access.get(lname);
        }

        /**
         * get list of locations (keys).
         * @return  array of locations
         */
        String[] getListLoc() {
            String[] out = new String[this.nV];
            return this.access.keySet().toArray(out);
        }

        /**
         * get list of locations class Loc (values).
         * @return  array of locations classes
         */
        Loc[] getListLocClass() {
            Loc[] out = new Loc[this.nV];
            return this.access.values().toArray(out);
        }

        /** 
         * get locations adjacent to location.
         * @param  lname the location name.
         * @return       a Loc iterator.
         */
        Iterator<Loc> getNeighbors(String lname) {
            return this.adjl.get(lname).listIterator();
        }
    }

    /**
     * Traffic class to store weights.
     * used as like a weight function.
     * Stores the data from map connections.
     */
    private static class Traffic {
        /** 
         * hashmap storing weights between two locations.
         * key:   String of form "([name1], [name2])"
         * value: Integer for travel time
         */
        Map<String, Integer> weights;

        /** def. constr. */
        Traffic() {
            this.weights = new HashMap<String, Integer>();
        }

        /** 
         * add connection.
         * @param s string from map connections file.
         * @param w the integer weight for the edge (travel time)
         * @return  string of indiv compoenent loc names
         */
        String[] addConnection(String s, int w) {
            //get to the form of: "([name1],[name2])" 
            //stripping whatever white space
            //and splitting on the comma of the ordered pair.
            String[] tmp = s.replaceAll("[()]", "").trim().split("\\s*,\\s*");
            String key = "(" + tmp[0] + ", " + tmp[1] + ")";
            
            //note undirected
            this.weights.put(key, w);
            return tmp;
        }

        /** 
         * get traffic between two locations.
         * @param  a location1
         * @param  b location2
         * @return   the travel time.
         */
        int getTime(String a, String b) {
            String key1 = "(" + a + ", " + b + ")";
            String key2 = "(" + b + ", " + a + ")";
            //test for two orderings, rather do this than
            //have a second entry for each ordering (save a lil space)
            if (this.weights.containsKey(key1)) {
                return this.weights.get(key1);
            } else if (this.weights.containsKey(key2)) {
                return this.weights.get(key2);
            } else {
                System.out.println("No traffic here.");
                System.exit(1);
                return 0;
            }

        }  
    }

    /**
     * Drivers corresponding to drivers at each Loc.
     * used to look up.
     * Stores the data from driver locations.
     */
    private static class Drivers {
        /** 
         * hashmap storing weights between two locations.
         * key:   String of location
         * value: LinkedList of Drivers
         */
        Map<String, LinkedList<Driver>> dr;
        /** total num drivers. */
        int numDrivers;
        /** total num locations. */
        int numLocs;

        /** def. constr. */
        Drivers() {
            this.dr = new HashMap<String, LinkedList<Driver>>();
            this.numDrivers = 0;
            this.numLocs = 0;
        }

        /** 
         * add an entry to hashmap.
         * @param l location string
         */
        void addLocation(String l) {
            this.dr.put(l, new LinkedList<Driver>());
            this.numLocs++;
        }

        /**
         * is there driver here.
         * @param  l location string.
         * @return  boolean t/f.
        */
        boolean hasDriver(String l) {
            return this.dr.get(l).size() > 0;
        }

        /**
         * add a driver d to a location l.
         * @param  id the ID
         * @param  l  string for loc name
         */
        void addDriver(int id, String l) {
            Driver d = new Driver(id, l);
            this.dr.get(l).add(d);

            this.numDrivers++;
            // System.out.println("Added Driver" + id+ "to" + l);
            // System.out.println("Now " + l + "has size: " 
            //                     + this.dr.get(l).size());
        }

        /**
         * numDrivers.
         * @return numDrivers
         */
        int numDrivers() {
            return this.numDrivers;
        }

        /**
         * get iterator to say hello to drivers at location l.
         * @param  l location string.
         * @return  driver iterator.
         */
        Iterator<Driver> getDriversIter(String l) {
            return this.dr.get(l).listIterator();
        }
    }

    /**
     * its a vertex on my graph.
     */
    private static class Loc implements Comparable<Loc> {
        /** loc name. */
        String name;
        /** min Dist to source. */
        Integer minD;
        /** Parent pointer. */
        Loc parent;
        
        /** default constr. */
        Loc() {
        }

        /** 
         * location name.
         * @param  n the name of the location.
         */
        Loc(String n) {
            this.name = n;
            this.minD = Integer.MAX_VALUE;
            this.parent = null;
        }

        /**
         * comparable.
         * @param  other other Loc (vertex)
         * @return  int for 0 if same, -1 if less, 1 if more
         */
        public int compareTo(Loc other) {
            //diff location, same minD, return -1
            if (this.name.compareTo(other.getName()) != 0) {
                if (this.minD == other.getD()) {
                    return -1;
                }
            } 
            return Integer.compare(this.minD, other.getD());
        }

        /**
         * getName of location.
         * @return  string for name.
         */
        public String getName() {
            return this.name;
        }

        /** 
         * set minD.
         * @param d for minD
         */
        public void setD(int d) {
            this.minD = d;
        }

        /**
         * set parent.
         * @param p for parent.
         */
        public void setP(Loc p) {
            this.parent = p;
        }

        /** 
         * get minD.
         * @return int for minD
         */
        public int getD() {
            return this.minD;
        }

        /**
         * get parent.
         * @return parent.
         */
        public Loc getP() {
            return this.parent;
        }
    }

    /**
     * Mah buddies.
     */
    private static class Driver {
        /** id. */
        Integer id;
        /** where am i. */
        String whereAt;
        /** how far from selected location. */
        Integer howFar;

        /** default constr. */
        Driver() {
        }

        /**
         * construct a driver.
         * @param  i the id
         * @param  where nearest location
         */
        Driver(int i, String where) {
            this.id = i;
            this.whereAt = where;
            this.howFar = null;
        }
        
        /** 
         * get id.
         * @return  int id
         */
        public int getId() {
            return this.id;
        }

        /** 
         * get loc.
         * @return  string where driver is.
         */
        public String whereRU() {
            return this.whereAt;
        }

        /**
         * set the distance from selected location.
         * when found with Dijkstra.
         * @param far the distance from selected location.
         */
        public void setDist(int far) {
            this.howFar = far;
        }
    }
}